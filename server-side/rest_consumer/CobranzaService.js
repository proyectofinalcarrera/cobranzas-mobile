/**
 * Módulo que ejecuta operaciones CRUD relacionadas con 
 * la entidad Cobranza sobre el core de ventas
 * 
 * @module AbastoService
 * 
 */

/** @type {Object} Módulo utilitario que brinda una interfaz
 	para consumir servicios REST */
var consumer = require('./RestConsumerUtil');
/** [obtenerAbastos Obtiene las últimas cobranzas del core de Ventas] */
exports.obtenerCobranzas = function (callback){
    console.log("Comienza la actualización de las cobranzas...");
   consumer.get("Cobranza", callback);
};
/** [crearCobranza Crea la cobranza que se le pasa como parámetro] */
exports.crearCobranza = function(cobranza, callback){
    consumer.post("Cobranza", cobranza, callback);
};
