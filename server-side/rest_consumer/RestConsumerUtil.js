/**
 * Módulo utilitario que consume servicios REST y brinda una interfaz
 * para realizar las operaciones HTTP
 * 
 * @module RestConsumerUtil
 * 
 */

/** @type {Object} Librería http */
var http = require('http');
/** @type {Object} Librería requesst */
var request = require('request');
/** @type {Object} Módulo principal */
var app = require('../app');
/** @type {String} URL de los servicios que brinda el core de ventas */
var url = app.settings.apiUrl;

/** [get Función utilitaria que permite hacer peticiones GET sobre
	la entidad que se le pasa como parámetro] */
exports.get = function (entidad,callback){ 
	http.get( url + entidad, 
   		function(response) {
			var body = '';
			//Arma la respuesta
			response.on('data', 
				function(d) {
					body += d;
				}
			);
			//Ejecuta cuando finaliza
			response.on('end', 
				function() {
					//Parsea la respuesta
					var Cuentas = JSON.parse(body);
					//Ejecuta la función que se pasa como callback
					callback(Cuentas);
				}
			);
		}
	)
	.on('error',
		function(e) {
   			console.log("Got error: " + e.message);
		}
	)
};
/** [get Función utilitaria que permite hacer peticiones POST sobre
	la entidad que se le pasa como parámetro y envía como payload "objeto"] */
exports.post = function(entidad, objeto, callback){
	request(
		//Información del request
		{
			url: url + entidad,
			method: "POST",
			json: true,   
			body: objeto
		},
		//Se ejecuta cuando se recibe la respuesta
		function (error, response, body){
			//Como la respuesta viene vacía el código es 204
			if (response && response.statusCode >= 200 && response.statusCode < 300) {
				console.info("Creado! Entidad: " + entidad)
				console.log(objeto);
				callback();
			}
			else{
				console.error("[ERROR]: " + response.statusCode);
				console.error("[ERROR]: " + response.body.ExceptionMessage);
				console.error("[ERROR]: " + response.body.ExceptionType);
				console.error("[ERROR]: " + response.body.StackTrace);
			}
		}
	);
}
