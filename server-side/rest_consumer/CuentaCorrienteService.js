/**
 * Módulo que ejecuta operaciones CRUD relacionadas con 
 * la entidad CuentaCorriente sobre el core de ventas
 * 
 * @module AbastoService
 * 
 */

/** @type {Object} Módulo utilitario que brinda una interfaz
 	para consumir servicios REST */
var consumer = require('./RestConsumerUtil');
/** [obtenerCuentasCorrientes Obtiene las cuentas corrientes de
	todos los clientes del core de Ventas] */
exports.obtenerCuentasCorrientes = function (callback){
    console.log("Comienza la actualización de las cuentas corrientes...");
    consumer.get("CuentaCorriente", callback); 
};
