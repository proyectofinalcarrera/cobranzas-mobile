/**
 * Módulo que ejecuta operaciones CRUD relacionadas con 
 * la entidad Cliente sobre el core de ventas
 * 
 * @module ClienteService
 * 
 */

/** @type {Object} Módulo utilitario que brinda una interfaz
 	para consumir servicios REST */
var consumer = require('./RestConsumerUtil');
/** [obtenerClientes Obtiene todos los clientes del core de Ventas] */
exports.obtenerClientes = function(callback){
   console.log("Comienza la actualización de los clientes...");
   consumer.get("Cliente", callback);  
};
