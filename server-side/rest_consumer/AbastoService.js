/**
 * Módulo que ejecuta operaciones CRUD relacionadas con 
 * la entidad Abasto sobre el core de ventas
 * 
 * @module AbastoService
 * 
 */

/** @type {Object} Módulo utilitario que brinda una interfaz
 	para consumir servicios REST */
var consumer = require('./RestConsumerUtil');
/** [obtenerAbastos Obtiene todos los abastos del core de Ventas] */
exports.obtenerAbastos = function (callback){
  console.log("Comienza la actualización de los abastos...");
  consumer.get("Abasto",callback);
};