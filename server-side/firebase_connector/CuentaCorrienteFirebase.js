/**
 * Módulo que ejecuta operaciones relacionadas con la entidad CuentaCorriente
 * en la BD Firebase
 * 
 * @module CuentaCorrienteFirebase
 * 
 */

/** @type {Object} Librería Firebase */
var Firebase = require('firebase');
/** @type {Object} Módulo principal */
var app = require('../app');
/** @type {Firebase} Referencia al documento de cuentas corrientes dentro de la BD */
var firebaseRef = new Firebase(app.settings.firebaseRef).child("cuentas-corrientes");

/** [eliminarTodo Elimina todas las cuentas corrientes de la BD Firebase] */
exports.eliminarTodo = function(){
	firebaseRef.remove();
}
/** [agregar Agrega un array de cuentas corrientes a la BD Firebase] */
exports.agregar = function(CuentasCorrientes){
	CuentasCorrientes.forEach(function(CuentaCorriente) {
		firebaseRef.child(CuentaCorriente.clienteID).set(CuentaCorriente);
	}, this);
	console.log("Las cuentas corrientes se actualizaron correctamente!");
}
