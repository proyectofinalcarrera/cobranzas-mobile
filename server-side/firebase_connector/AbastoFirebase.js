/**
 * Módulo que ejecuta operaciones relacionadas con la entidad Abasto
 * en la BD Firebase
 * 
 * @module AbastoFirebase
 * 
 */

/** @type {Object} Librería Firebase */
var Firebase = require('firebase');
/** @type {Object} Módulo principal */
var app = require('../app');
/** @type {Firebase} Referencia al documento de abastos dentro de la BD */
var firebaseRef = new Firebase(app.settings.firebaseRef).child("abastos");

/** [eliminarTodo Elimina todos los abastos de la BD Firebase] */
exports.eliminarTodo = function(){
	firebaseRef.remove();
}
/** [agregar Agrega un array de abastos a la BD Firebase] */
exports.agregar = function(Abastos){
	Abastos.forEach(function(Abasto) {
		firebaseRef.child(Abasto.ID).set(Abasto);
	}, this);
	console.log("Los abastos se actualizaron correctamente!");
}
