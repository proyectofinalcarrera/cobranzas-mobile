/**
 * Módulo que ejecuta operaciones relacionadas con la entidad Cliente
 * en la BD Firebase
 * 
 * @module ClienteFirebase
 * 
 */

/** @type {Object} Librería Firebase */
var Firebase = require('firebase');
/** @type {Object} Módulo principal */
var app = require('../app');
/** @type {Firebase} Referencia al documento de clientes dentro de la BD */
var firebaseRef = new Firebase(app.settings.firebaseRef).child("clientes");

/** [eliminarTodo Elimina todos los clientes de la BD Firebase] */
exports.eliminarTodo = function(){
	firebaseRef.remove();
}
/** [agregar Agrega un array de clientes a la BD Firebase] */
exports.agregar = function(Clientes){
	Clientes.forEach(function(Cliente) {
		firebaseRef.child(Cliente.ID).set(Cliente);
	}, this);
	console.log("Los clientes se actualizaron correctamente!");
}
