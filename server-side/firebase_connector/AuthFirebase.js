/**
 * Módulo que ejecuta operaciones relacionadas con el login
 * en la BD Firebase
 * 
 * @module AuthFirebase
 * 
 */

/** @type {Object} Librería Firebase */
var Firebase = require('firebase');
/** @type {Object} Módulo principal */
var app = require('../app');
/** @type {Firebase} [description] */
var firebaseRef = new Firebase(app.settings.firebaseRef);

/** [login Realiza la autenticación 
	para poder operar sobre la BD  Firebase] */
exports.login = function () {
	var username = app.settings.username;
	var password = app.settings.password;

	firebaseRef.authWithPassword({
		email: username,
		password: password
	}, function (error, authData) {  
		if(authData !== null) console.log("Login OK! uid", authData.uid);
		if(error !==null) console.log("Login Failed!", error);
	}, {
			remember: "sessionOnly"
		});
}