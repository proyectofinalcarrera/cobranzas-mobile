/**
 * Módulo que ejecuta operaciones relacionadas con la entidad Cobranza
 * en la BD Firebase
 * 
 * @module CobranzaFirebase
 * 
 */

/** @type {Object} Librería Firebase */
var Firebase = require('firebase');
/** @type {Object} Módulo principal */
var app = require('../app');
/** @type {Firebase} Referencia al documento de cobranzas dentro de la BD */
var firebaseRef = new Firebase(app.settings.firebaseRef).child("cobranzas");

/** [eliminarTodo Elimina todas las cobranzas de la BD Firebase] */
exports.eliminarTodo = function(){
	firebaseRef.remove();
}
/** [agregar Agrega un array de cobranzas a la BD Firebase] */
exports.agregar = function(Cobranzas){
	Cobranzas.forEach(function(Cobranza) {
		firebaseRef.child(Cobranza.ID).set(Cobranza);
	}, this);
	console.log("Las cobranzas se actualizaron correctamente!");
}
/** [marcarComoGuardada Marca como guardada una cobranza] */
exports.marcarComoGuardada = function(CobranzaId){
	firebaseRef.child(CobranzaId).update({guardado:true},
	function(){
		console.log("La cobranza se ha marcado como guardada!");
	});
}
