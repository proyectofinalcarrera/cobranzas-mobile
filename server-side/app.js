var express = require('express');
var path = require('path');
var fs = require('fs');

var app = express();
var path = 'config/application-' + app.get('env') + '.json';
app.settings = JSON.parse(fs.readFileSync(path, 'utf8'));

module.exports = app;

var listener = require('./trigger/listener');
var scheduler = require('./trigger/scheduler');
var rest = require('./trigger/rest');
app.use('/', rest);
app.use('/update',rest);



