/**
 * Módulo que provee métodos para actualizar las distintas entidades
 * de la BD Firebase
 * 
 * @module updater
 * 
 */

/**
 * Busca todos los módulos que consumen servicios web del core de ventas
 */
var CuentaCorrienteService = require('../rest_consumer/CuentaCorrienteService');
var CobranzaService = require('../rest_consumer/CobranzaService');
var AbastoService = require('../rest_consumer/AbastoService');
var ClienteService = require('../rest_consumer/ClienteService');
/**
 * Busca todos los módulos que se encargar de interactuar con la BD Firebase
 */
var AbastoFirebase = require('../firebase_connector/AbastoFirebase');
var ClienteFirebase = require('../firebase_connector/ClienteFirebase');
var CobranzaFirebase = require('../firebase_connector/CobranzaFirebase');
var CuentaCorrienteFirebase = require('../firebase_connector/CuentaCorrienteFirebase');
var AuthFirebase = require('../firebase_connector/AuthFirebase');

/** [actualizarAbastos Elimina todos los abastos en la BD Firebase
	y busca y agrega todos los abastos nuevamente] */
exports.actualizarAbastos = function(){
	AbastoFirebase.eliminarTodo();
	AbastoService.obtenerAbastos(AbastoFirebase.agregar);
}
/** [actualizarClientes Elimina todos los clientes en la BD Firebase
	y busca y agrega todos los clientes nuevamente] */
exports.actualizarClientes = function(){
	ClienteFirebase.eliminarTodo();
	ClienteService.obtenerClientes(ClienteFirebase.agregar);
}
/** [actualizarCobranzas Elimina todas las cobranzas en la BD Firebase
	y busca y agrega todas las cobranzas nuevamente] */
exports.actualizarCobranzas = function(){
	CobranzaFirebase.eliminarTodo();
	CobranzaService.obtenerCobranzas(CobranzaFirebase.agregar);
}
/** [actualizarCuentasCorrientes Elimina todos las cuentas corrientes en la BD Firebase
	y busca y agrega todos los cuentas corrientes nuevamente] */
exports.actualizarCuentasCorrientes = function(){
	CuentaCorrienteFirebase.eliminarTodo();
	CuentaCorrienteService.obtenerCuentasCorrientes(CuentaCorrienteFirebase.agregar);
}
