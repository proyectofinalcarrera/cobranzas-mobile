/**
 * Módulo que programa eventos
 * 
 * @module scheduler
 * 
 */

/** @type {Object} Librería cron */
var CronJob = require('cron').CronJob;

var Updater = require('./updater');
var AuthFirebase = require('../firebase_connector/AuthFirebase');

/** Actualiza los abastos todos los días de Lunes a Viernes a las 23:00:00 */
new CronJob('0 0 23 * * 1-5', Updater.actualizarAbastos, null, true, 'America/Buenos_Aires');
/** Actualiza los clientes todos los días de Lunes a Viernes a las 23:00:05 */
new CronJob('5 0 23 * * 1-5', Updater.actualizarClientes, null, true, 'America/Buenos_Aires');
/** Actualiza las cobranzas todos los días de Lunes a Viernes a las 23:00:10 */
new CronJob('10 0 23 * * 1-5', Updater.actualizarCobranzas, null, true, 'America/Buenos_Aires');
/** Actualiza las cuentas corrientes todos los días de Lunes a Viernes a las 23:00:15 */
new CronJob('15 0 23 * * 1-5', Updater.actualizarCuentasCorrientes, null, true, 'America/Buenos_Aires');
/** Autentica cada 5 minutos */
new CronJob('0 */5 * * * *', AuthFirebase.login, null, true, 'America/Buenos_Aires');