/**
 * [consumer Módulo que permite escuchar por nuevas cobranzas en la BD Firebase
 * y crear los pagos en el core de ventas]
 * 
 */

/**
 * Instancia librerías y módulos
 */
var Firebase = require('firebase');
var consumer = require('../rest_consumer/CobranzaService');
var CobranzaFirebase = require('../firebase_connector/CobranzaFirebase');
var AuthFirebase = require('../firebase_connector/AuthFirebase');
var app = require('../app');

/**
 * [firebaseRef Busca el path de la BD Firebase y crea la referencia]
 * @type {Firebase}
 */
var firebaseRef = new Firebase(app.settings.firebaseRef);

AuthFirebase.login();
/**
 * [Ejecuta una función cada vez que aparece una entidad nueva en la sección cobranzas]
 */
firebaseRef.child('cobranzas').orderByChild('guardado').equalTo(false).on('child_added',
	function(cobranzaData, prevChildKey) {
		//Convierte la nueva cobranza a un objeto JS
		var cobranza = cobranzaData.val();
		cobranza.pagos = [];
		//Busca los pagos de la cobranza y los guarda en la cobranza
		firebaseRef.child("cobranzas").child(cobranzaData.key()).child("pagos").once('value', 
			function(data){
				data.forEach(
					function(pagoData) {
						var pago = pagoData.val();
						cobranza.pagos[cobranza.pagos.length] = pago;
					}	
				);
			}
		);
		//Manda a crear la cobranza y luego marca la cobranza como finalizada
		consumer.crearCobranza(cobranza,
			function(){
				CobranzaFirebase.marcarComoGuardada(cobranza.ID);
			}
			);
	}
);

