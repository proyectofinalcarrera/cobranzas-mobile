/**
 * Módulo que ofrece una API para poder actualizar
 *  la BD Firebase cuando se desee
 * 
 * @module rest
 * 
 */

var express = require('express');
var router = express.Router();
var Updater = require('./updater');

/**
 * Servicio que permite actualizar los abastos
 */
router.get('/abastos', function(req, res) {
	Updater.actualizarAbastos();
	res.send('Los ABASTOS se han actualizado');
});

/**
 * Servicio que permite actualizar los clientes
 */
router.get('/clientes', function(req, res) {
	Updater.actualizarClientes();
	res.send('Los CLIENTES se han actualizado');
});

/**
 * Servicio que permite actualizar las cobranzas
 */
router.get('/cobranzas', function(req, res) {
	Updater.actualizarCobranzas();
	res.send('Las COBRANZAS se han actualizado');
});

/**
 * Servicio que permite actualizar las cuentas corrientes
 */
router.get('/cuentascorrientes', function(req, res) {
	Updater.actualizarCuentasCorrientes();
	res.send('Las CUENTAS CORRIENTES se han actualizado');
});



module.exports = router;
