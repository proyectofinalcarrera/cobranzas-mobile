// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('cobranza-mobile', ['ionic', 'cobranza-mobile.controllers','ionic-material','firebase','cobranza-mobile.services','angularMoment'])

.run(function($ionicPlatform,$http,$rootScope) {
  
  var env = 'dev';
  
  var filename = 'config/application-' + env + '.json';
  $http.get(filename).success(function(data) {
    $rootScope.settings = data
  });
    
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      window.open = cordova.InAppBrowser.open;
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

  .state('app', {
    url: "/app",
    abstract: true,
    templateUrl: "views/menu.html",
    controller: 'AppCtrl'
  })

  .state('app.clientes', {
    url: "/clientes",
    views: {
      'menuContent': {
        templateUrl: "views/cliente/list.cliente.html",
        controller: 'ClientesCtrl'
      }
    }
  })
  
  .state('app.single', {
    url: "/clientes/:clienteId",
    views: {
      'menuContent': {
        templateUrl: "views/cliente/see.cliente.html",
        controller: 'ClienteCtrl'
      }
    }
  })
  .state('app.cobranzas', {
    url: "/cobranzas",
    views: {
      'menuContent': {
        templateUrl: "views/cobranza/list.cobranza.html",
        controller: 'ListarCobranzasCtrl'
      }
    }
  }).state('app.cobranza', {
    url: "/cobranzas/:cobranzaId",
    views: {
      'menuContent': {
        templateUrl: "views/cobranza/see.cobranza.html",
        controller: 'VerCobranzaCtrl'
      }
    }
  }).state('app.cobranza-actual', {
    url: "/cobranza-actual",
    views: {
      'menuContent': {
        templateUrl: "views/cobranza/current.cobranza.html",
        controller: 'CobranzaCtrl'
      }
    }
  }).state('app.cuenta-corriente', {
    url: "/cuenta-corriente/:clienteId",
    views: {
      'menuContent': {
        templateUrl: "views/cuentacorriente/see.cuentacorriente.html",
        controller: 'CuentaCorrienteCtrl'
      }
    }
  }).state('app.login', {
    url: "/login",
    views: {
      'menuContent': {
        templateUrl: "views/login.html",
        controller: 'AppCtrl'
      }
    }
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/login');
});
