angular.module('cobranza-mobile.services')
	/**
	 * @ngdoc service
	 * @name cobranza-mobile.services:AuthService
	 * 
	 * @requires $rootScope
	 *
	 * @description
	 * Gestiona las operaciones relacionadas con la autenticación de usuario
	 */
	.factory('AuthService',
		function (
			$rootScope, $location) {

			var firebaseRef = new Firebase($rootScope.settings.firebaseRef);
			firebaseRef.onAuth(
				function(authData) {
					if (authData) {
						console.log("Authenticated with uid:", authData.uid);
					} 
					else {
						console.log("Client unauthenticated.");
						$rootScope.loginVar = false;
						$location.url("app/login");
					}
				}
			);

			var methods = 
			{
				getAuth: function () {
					return firebaseRef.getAuth();
				},
				login: function (
					email, password, callbackFailure, callbackSuccess) {
					console.log(email,password);
					firebaseRef.authWithPassword({
					  email    : email,
					  password : password
					}, callbackFailure);
				}
			};
			return methods;
		}
	);