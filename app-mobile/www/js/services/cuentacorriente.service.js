
angular.module('cobranza-mobile.services')
	/**
	 * @ngdoc service
	 * @name cobranza-mobile.services:CuentaCorrienteService
	 * 
	 * @requires $rootScope
	 *
 	 * @requires $firebaseArray
	 * @requires $firebaseObject

	 * @description
	 * Gestiona las operaciones relacionadas con las cuentas corrientes de los clientes
	 */
	.factory('CuentaCorrienteService',
		function(
			$rootScope,
			$firebaseArray,$firebaseObject) {
			//Crea la referencia a Firebase
			var ref = new Firebase($rootScope.settings.firebaseRef).child("cuentas-corrientes");
			//Define el objeto con los métodos
			var methods = {
				/**
				* @ngdoc method
				* @name find
				*
				* @description Busca la cuenta corriente del cliente que se pasa como parámetro
				* @param {int} clienteId ID del cliente
				* @returns {Object} Cuenta corriente del cliente
				*
				*/
				find: function(clienteId){
					return $firebaseObject(ref.child(clienteId));
				}
			};
			return methods;
		}
		
	);