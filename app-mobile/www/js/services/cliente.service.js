angular.module('cobranza-mobile.services', [])
	.factory('ClienteService',
		function (
			$rootScope,
			$firebaseArray, $firebaseObject) {

			var ref = new Firebase($rootScope.settings.firebaseRef).child("clientes");

			var methods = {
				listAll: function () {
					return $firebaseArray(ref);
				},
				listAllFrom: function(
					abastoID){
					return $firebaseArray(ref.orderByChild("abastoID").equalTo(Number(abastoID))); 
				},			
				find: function (
					key) {
					return $firebaseObject(ref.child(key));
				}
			};
			
			return methods;
		}
	);