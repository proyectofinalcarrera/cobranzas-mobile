angular.module('cobranza-mobile.services')
	/**
	 * @ngdoc service
	 * @name cobranza-mobile.services:AbastoService
	 * 
	 * @requires $rootScope
	 *
	 * @requires $firebaseArray
	 * @requires $firebaseObject
	 * 
	 * @description
	 * Gestiona las operaciones relacionadas con los abastos
	 */
	.factory('AbastoService', 
		function(
			$rootScope,
			$firebaseArray, $firebaseObject) {
			//Crea la referencia a Firebase
			var ref = new Firebase($rootScope.settings.firebaseRef).child("abastos");
			//Define el objeto con los métodos
			var methods = {
				/**
				* @ngdoc method
				* @name listAll
				*
				* @description Busca todos los abastos
				* @returns {Array} Array de abastos
				*
				*/
				listAll: function(){
					return $firebaseArray(ref);
				},
				/**
				* @ngdoc method
				* @name find
				*
				* @description Busca el abasto con el ID que se pasa como parámetro
				* @param {int} abastoId ID del abasto
				* @returns {Object} Abasto que posee el ID
				*
				*/
				find: function(abastoId){
					var abasto = $firebaseObject(ref.child(abastoId));
					return abasto;
				}
			};
			return methods;
		}
	
	);