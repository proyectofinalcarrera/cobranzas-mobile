angular.module('cobranza-mobile.services')
	/**
	 * @ngdoc service
	 * @name cobranza-mobile.services: PagoService
	 * 
	 * @requires $rootScope
	 * 
	 * @requires AuthService
	 * 
	 * @requires $firebaseArray
	 * @requires $firebaseObject
	 * 
	 * @description
	 * Gestiona las operaciones relacionadas con los pagos de
	 * la cobranza actual
	 * 
	 */
	.factory('PagoService',
		function(
			$rootScope,
			AuthService,
			$firebaseArray, $firebaseObject) {

			var ref = new Firebase($rootScope.settings.firebaseRef)
				.child("cobranza-actual")
				.child(AuthService.getAuth().uid);

			var methods = {
				/**
				* @ngdoc method
				* @name listAllFrom
				*
				* @description Lista todos los pagos de un cliente 
				* en la cobranza actual
				* @param {int} clienteId ID del cliente
				* @returns {Array} Lista de pagos
				*
				*/
				listAllFrom: function(clienteId){
					return $firebaseArray(ref.child("pagos")
						.orderByChild("clienteID")
						.equalTo(parseInt(clienteId))
						);
				},
				/**
				* @ngdoc method
				* @name listAll
				*
				* @description Lista todos los pagos de la cobranza actual
				* @returns {Array} Lista de todos los pagos
				*
				*/
				listAll: function(){
					return $firebaseArray(ref.child("pagos"));
				},
				/**
				* @ngdoc method
				* @name addPago
				*
				* @description Agrega un pago a la cobranza actual
				* @param {Object} pago Pago a agregar
				*
				*/
				addPago: function(pago){
					//Busca la cobranza actual
					var cobranza = $firebaseObject(ref);
					//Una vez abierta...
					cobranza.$loaded().then(function(){
						//Le suma el monto del pago actual al totalRecaudado
						//de la cobranza actual
						cobranza.totalRecaudado += pago.monto;
						//Guarda el cambio
						cobranza.$save();
						//Busca los pagos de la cobranza actual
		                var pagos = $firebaseArray(ref.child("pagos"));
		                //Una vez encontrados...
	                	pagos.$loaded().then(function(){
	                		//Agrega el pago
	                    	pagos.$add(pago);
	                	});
					});
				},
				/**
				* @ngdoc method
				* @name removePago
				*
				* @description Elimina un pago de la cobranza actual
				* @param {Object} pago Pago a eliminar
				*
				*/
				removePago: function(pago){
					var cobranza = $firebaseObject(ref);
					cobranza.$loaded().then(
						function(){
							cobranza.totalRecaudado -= pago.monto;
							cobranza.$save();
							ref.child("pagos").child(pago.$id).remove();
						}
					);
				},
				/**
				* @ngdoc method
				* @name editPago
				*
				* @description Modifica los valores de un pago
				* @param {String} key ID del pago
				* @param {double} monto Nuevo monto del pago
				* @param {int} clienteID Nuevo cliente del pago
				* @param {String} clienteNombre Nombre del nuevo cliente
				*/				
				editPago: function(key, monto, clienteID, clienteNombre){
					//Busca el pago a modificar
					var pago = $firebaseObject(ref.child("pagos").child(key));
					pago.$loaded().then(
						function(){
							//Busca la cobranza
							var cobranza = $firebaseObject(ref);
							cobranza.$loaded().then(
								function(){
									//Resta la diferencia entre los moentos del pago
									//viejo y nuevo al total recaudado de la cobranza
									cobranza.totalRecaudado -= (pago.monto - monto);
									//Guarda el cambio en la cobranza actual
									cobranza.$save();
									//Modifica los valores del pago
									pago.monto = monto;
									pago.clienteID = clienteID;
									pago.clienteNombre = clienteNombre;
									//Guarda los cambios del pago
									pago.$save();	
								}
							);		
						}
					);
				}
			};
			return methods;
		}
		
	);