angular.module('cobranza-mobile.services')
	/**
	 * @ngdoc service
	 * @name cobranza-mobile.services: CobranzaService
	 * 
	 * @requires $rootScope
	 * 
	 * @requires AuthService
	 * 
	 * @requires $firebaseArray
	 * @requires $firebaseObject
	 * 
	 * @description
	 * Gestiona las operaciones relacionadas con la cobranza actual y 
	 * cobranzas anteriores
	 */
	.factory('CobranzaService',
		function(
			$rootScope,
			AuthService,
			$firebaseArray, $firebaseObject) {
			var ref = new Firebase($rootScope.settings.firebaseRef);
			var methods = {
				/**
				* @ngdoc method
				* @name getCurrentCobranza
				*
				* @description Busca la cobranza actual del usuario logueado
				* @returns {Object} Cobranza actual
				*
				*/
				getCurrentCobranza: function(){
					//Busca la información del usuario logueado
					var auth = AuthService.getAuth();
					return $firebaseObject(ref.child("cobranza-actual").child(auth.uid));
				},
				/**
				* @ngdoc method
				* @name listAll
				*
				* @description Lista todas las cobranzas exceptuando la actual
				* @returns {Array} Lista de cobranzas
				*
				*/
				listAll: function(){	
	   				return $firebaseArray(ref.child("cobranzas").orderByChild("fecha"));
				},
				/**
				* @ngdoc method
				* @name find
				*
				* @description Busca la cobranza que posee el ID pasado como parámetro
				* @param {int} abastoId ID del abasto
				* @returns {Object} Cobranza que posee el ID
				*
				*/
				find: function(cobranzaId){
					return $firebaseObject(ref.child("cobranzas").child(cobranzaId));
				},
				/**
				* @ngdoc method
				* @name finalizarCobranza
				*
				* @description Finaliza la cobranza actual
				*
				*/
				finalizarCobranza: function(){
					var auth = AuthService.getAuth();
					//Busca la cobranza actual
					ref.child("cobranza-actual").child(auth.uid).once("value",
						function(snapshot) {
		  					var cobranzaActual = snapshot.val();
		  					//Agrega la cobranza actual a cobranzas
							ref.child("cobranzas").push(cobranzaActual);
							cobranzaActual = {};
							//Setea un elemento vació a cobranz actual (la elimina)
							ref.child("cobranza-actual").child(auth.uid).set(cobranzaActual);
						}, 
						function (errorObject) {
	  						console.log("The read failed: " + errorObject.code);
						}
					);
				},
				/**
				* @ngdoc method
				* @name startCobranza
				*
				* @description Inicia una cobranza nueva y finaliza si hay una actual
				* @param {Object} cobranza 
				*
				*/
				startCobranza: function(cobranza){
					var auth = AuthService.getAuth();
					//Busca la cobranza actual
					ref.child("cobranza-actual").child(auth.uid).once("value",
						function(snapshot) {
		  					var cobranzaActual = snapshot.val();
		  					//Si existe cobranza actual, la agrega a cobranzas
							if(cobranzaActual !== null){
								ref.child("cobranzas").push(cobranzaActual);					  
							}
							//Crea la nueva cobranza actual
							ref.child("cobranza-actual").child(auth.uid).set(cobranza);		
						}
					)
				},
				/**
				* @ngdoc method
				* @name modificarCobranza
				*
				* @description Modifica la fecha de la cobranza actual
				* @param {String} fecha 
				*
				*/
				modificarCobranza: function(fecha){
					var auth = AuthService.getAuth();
					ref.child("cobranza-actual").child(auth.uid).update({fecha: fecha});
				},
				/**
				* @ngdoc method
				* @name cancelarCobranza
				*
				* @description Elimina la cobranza actual
				*
				*/
				cancelarCobranza: function(){
					var auth = AuthService.getAuth();
					ref.child("cobranza-actual").child(auth.uid).set({});
				}
			};
			return methods;
		}
		
	);