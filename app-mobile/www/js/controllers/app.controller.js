angular.module('cobranza-mobile.controllers',[])
	/**
	 * @ngdoc controller
	 * @name cobranza-mobile.controllers:AppCtrl
	 * 
	 * @requires $scope
	 * @requires $timeout
	 * @requires $rootScope
	 * @requires $http
	 * @requires $location
	 * @requires $window
	 * @requires $ionicPopup
	 * @requires $ionicHistory
	 * 
	 * @requires CobranzaService
	 * @requires AuthService
	 *
	 * 
	 * @description
	 * Controla la información y eventos de la app cuando inicia
	 */
	.controller('AppCtrl',
		function (
			$ionicPopup, $ionicHistory,
			$scope, $timeout, $rootScope, $http, $location, $window,
			CobranzaService, AuthService) {
			$rootScope.loginVar = false;
			$ionicHistory.nextViewOptions(
				{
					disableAnimate: true,
					disableBack: true
				}
			);
			/**
			* @ngdoc function
			* @name openModal
			*
			* @description Muestra la ventana modal
			*
			*/
			$scope.openModal = function () {
				$scope.modal.show();
			};
			/**
			* @ngdoc function
			* @name login
			*
			* @description Cierra la ventanta modal
			*
			*/
			$scope.login = function (email,password) {
				email = typeof email == 'undefined' ? "" : email;
				password = typeof password == 'undefined' ? "" : password;
				AuthService.login(
					email,
					password,
					function(error, authData) {
					  if (error) {
					  	console.log("ERRORRR");
					  	$scope.msg=error.code;
					  	$scope.$apply();
					}},
					function(){
						$rootScope.loginVar = true;
						$location.url("app/clientes");
						$scope.$apply();
					}
				);
			};

			var auth = AuthService.getAuth();
			if(auth !== null){
				$rootScope.loginVar = true;
				$location.url("app/clientes");
			}
		}
		);
