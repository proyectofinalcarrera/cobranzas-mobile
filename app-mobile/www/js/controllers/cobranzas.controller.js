angular.module('cobranza-mobile.controllers')
	/**
	 * @ngdoc controller
	 * @name cobranza-mobile.controllers:CobranzasCtrl
	 * 
	 * @requires $scope
	 * @requires $filter
	 * @requires location
	 * @requires $rootScope
	 *
	 * @requires CobranzaService
	 * @requires AbastoService
	 *
	 * @requires $ionicPopup
	 * @requires $ionicHistory
	 * @requires moment
	 * @requires $ionicModal
	 * 
	 * @description
	 * Controla la información y eventos relacionados a una cobranza
	 */
	.controller('CobranzasCtrl',
		function (
			$scope, $filter, $rootScope, $location,
			CobranzaService, AbastoService,
			$ionicPopup, $ionicHistory, moment, $ionicModal) {
			//Valores iniciales de la cobranza para iniciar una cobranza nueva
			$scope.cobranza = {abastoSelected: -1, fecha: new Date()};
			//Instancia la ventana modal que permite crear una cobranza nueva
			$ionicModal.fromTemplateUrl('views/cobranza/new.cobranza.html', {
				scope: $scope,
				animation: 'slide-in-up'
				}).then(
					function (modal) {
					$scope.modal = modal;
				}
			);
			/**
			* @ngdoc function
			* @name closeModal
			*
			* @description Cierra la ventana modal y limpia los campos
			*
			*/			
			$scope.closeModal = function () {
				$scope.modal.hide();
				$scope.cleanModal();
			};
			/**
			* @ngdoc function
			* @name iniciarCobranza
			*
			* @description Muestra la ventana modal para cargar los datos 
			* de la nueva cobranza
			*
			*/
			$scope.iniciarCobranza = function(){
				//Busca todos los abastos para que el usuario elija uno
				$scope.abastos = AbastoService.listAll();
				//Muestra la ventana modal
				$scope.modal.show();
			}	
			/**
			* @ngdoc function
			* @name aceoarCobranza
			*
			* @description Muestra la ventana modal para cargar los datos 
			* de la nueva cobranza
			*
			*/
			$scope.aceptarNuevaCobranza = function(){		

				var fecha = $scope.cobranza.fecha;

				var abastoID = $scope.cobranza.abastoSelected;
				var mensaje = "";
				var validado = true;

				var stringDate = moment(fecha).locale('es').format('YYYY-MM-DD');
				//Comienzo de validaciones
				if(isNaN(fecha) || fecha === null){
					validado = false;
					mensaje = mensaje + "Debe ingresar una fecha.<br>";
				}
				if(abastoID<0){
					validado = false;
					mensaje = mensaje + "Debe seleccionar una cobranza.";	
				}
				//Fin de validaciones
				if(validado){
					//Cierra la ventana modal
					$scope.closeModal();
					//Busca el abasto 
					var abasto = AbastoService.find(abastoID);
				 	abasto.$loaded().then(
				 		function(){
				 			//Crea el objeto cobranza
							var cobranza = 
								{
									abasto: abasto.nombre, 
									abastoID: abasto.ID,
									fecha: stringDate,
									guardado: false,
									totalRecaudado: 0 
								};
							//Inicia la cobranza
							CobranzaService.startCobranza(cobranza);
							//Elimina el historial de la próxima pantalla
							//para que sea root
							$ionicHistory.nextViewOptions(
								{
				  					disableAnimate: true,
				  					disableBack: true
								}
							);
							//Se utiliza para ocultar/mostrar opciones 
							//del menú izquierdo principal
							$rootScope.existeCobranza = true;
							//Se redirige a la vista cobranza-actual
							$location.url("app/cobranza-actual");
						}
					);
				}
				else{
					//Muestra mensaje si alguún dato es incorrecto
					$ionicPopup.alert(
						{
							title: "Datos incorrectos",
							template: mensaje
						}
					);
				}
				
			};
			/**
			* @ngdoc function
			* @name selected
			*
			* @description Selecciona una cobranza
			* @example $scope.selected(1)
			* @param {int} key ID del abasto que seleccionó el usuario
			*
			*/
			$scope.selected = function(key){
				$scope.cobranza.abastoSelected = key;	
			};
			/**
			* @ngdoc function
			* @name cleanModal
			*
			* @description Limpia los campos de la ventana modal
			*
			*/
			$scope.cleanModal = function(){
				$scope.cobranza.abastoSelected = -1;	
				$scope.cobranza.monto = "";
			};
			/**
			* @ngdoc function
			* @name finalizarCobranza
			*
			* @description Finaliza la cobranza actual
			*
			*/
			$scope.finalizarCobranza = function(){
					//Muestra un mensaje que solicita confirmación de la acción
					$ionicPopup.alert(
					{
						title: "Confirmar",
						template: "Presione OK para finalizar la cobranza actual",		
						buttons: 
						[
							{
								text: '<b>OK</b>',
			    				type: 'button-positive',
			    				onTap: function(e) {

			    					//Invoca el método que lleva a cabo la 
			    					//finalización de la cobranza actual
									CobranzaService.finalizarCobranza();
									//Elimina el historial para que la próxima 
									//ventana sea root
									$ionicHistory.nextViewOptions(
										{
			  								disableAnimate: true,
			  								disableBack: true
										}
									);
									//Se utiliza para ocultar/mostrar opciones
									//en el menú principal izquierdo
									$rootScope.existeCobranza = false;
									//Se dirige a la vista de cobranzas
									$location.url("app/cobranzas");
			    					}
		  					},
							{
								text: 'No',
								onTap: function(e) {  }
							}
						]
					}
				);
			
			}

		}
	)
	/**
	 * @ngdoc controller
	 * @name cobranza-mobile.controllers:ListarCobranzasCtrl
	 * 
	 * @requires $scope
	 *
	 * @requires CobranzaService
	 * 
	 * @requires $ionicHistory
	 * @requires moment
	 * 
	 * @description
	 * Controla la información y eventos de la vista de cobranzas
	 */
	.controller('ListarCobranzasCtrl',
		function (
			$scope,
			CobranzaService,
			$ionicHistory, moment) {
			//Busca todas las cobranzas, exceptuando la actual
			$scope.cobranzas = CobranzaService.listAll();
			/**
			* @ngdoc function
			* @name cantidadPagos
			*
			* @description Cuenta la cantidad de pagos que tiene una cobranza
			* @example $scope.cantidadPagos({cobranzaID: 2, pagos: { pago1, pago2 }})
			* @param {Object} cobranza Cobranza a la cual se le cuentan los pagos
			* @returns {int} Cantidad de pagos de la cobranza
			*
			*/
			$scope.cantidadPagos = function(cobranza){
				return Object.keys(cobranza.pagos).length;
			}
			/**
			* @ngdoc function
			* @name formatFecha
			*
			* @description Le da formato a una fecha para visualizar en pantalla
			* @example $scope.formatFecha("2015-05-24")
			* @param {String} fecha Fecha en formato "YYYY-MM-DD"
			* @returns {String} Fecha con formato
			*
			*/
			$scope.formatFecha = function(fecha){
				return moment(fecha, "YYYY-MM-DD").locale('es').format("dddd, DD/MM");		
			}
		}
	)
	/**
	 * @ngdoc controller
	 * @name cobranza-mobile.controllers:VerCobranzasCtrl
	 * 
	 * @requires $scope
	 * @requires $stateParams
	 * 
	 * @requires CobranzaService
	 * 
	 * @requires moment
	 * 
	 * @description
	 * Controla la información y eventos de la vista de cualquier cobranza
	 * que no sea la actual
	 */
	.controller('VerCobranzaCtrl',
		function (
			$scope, $stateParams,
			CobranzaService,
			moment) {
			//Obtiene el ID de la cobranza que se pasa como parámetro
			var cobranzaId = $stateParams.cobranzaId;
			//Busca la cobranza 
			$scope.cobranza = CobranzaService.find(cobranzaId);
			/**
			* @ngdoc function
			* @name formatFecha
			*
			* @description Le da formato a una fecha para visualizar en pantalla
			* @example $scope.formatFecha("2015-05-24")
			* @param {String} fecha Fecha en formato "YYYY-MM-DD"
			* @returns {String} Fecha con formato
			*
			*/
			$scope.formatFecha = function(fecha){
				return moment(fecha, "YYYY-MM-DD").locale('es').format("dddd, L");		
			}
		}
	);