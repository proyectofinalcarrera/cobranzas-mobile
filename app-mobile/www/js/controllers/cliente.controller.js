angular.module('cobranza-mobile.controllers')
	/**
	 * @ngdoc controller
	 * @name cobranza-mobile.controllers:ClienteCtrl
	 * 
	 * @requires $scope
	 * @requires $stateParams
	 *
	 * @requires ClienteService
	 * 
	 * @description
	 * Controla la información y eventos de la vista de cliente
	 */
	.controller('ClienteCtrl',
		function (
			$scope, $stateParams,
			ClienteService) {
			//ID del cliente pasado como parámetro en la URL
			var clienteId = $stateParams.clienteId;
			//Busca los datos del cliente
			$scope.cliente = ClienteService.find(clienteId);
		}
	);