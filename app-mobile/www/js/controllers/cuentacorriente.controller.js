angular.module('cobranza-mobile.controllers')
	/**
	 * @ngdoc controller
	 * @name cobranza-mobile.controllers:CuentaCorrienteCtrl
	 * 
	 * @requires $scope
	 * @requires $filter
	 * @requires $stateParams
	 *
	 * @requires CuentaCorrienteService
	 * @requires PagoService
	 * 
	 * @requires moment
	 * 
	 * @description
	 * Controla la información y eventos de la vista de cuenta corriente
	 */
	.controller('CuentaCorrienteCtrl', 
		function (
			$scope, $filter, $stateParams,
			CuentaCorrienteService, PagoService,
			moment) {
			//ID del cliente pasado como parámetro en la URL
			var clienteId = $stateParams.clienteId;
			//Busca la cuenta corriente del cliente
			$scope.cuentaCorriente = CuentaCorrienteService.find(clienteId);
			//Busca los pagos del cliente en la cobranza actual
			$scope.pagos = PagoService.listAllFrom(clienteId);
			//Fecha de los pagos de la cobranza actual
			$scope.fecha = "Hoy";
			/**
			* @ngdoc function
	 		* @name obtenerSuma
	 		*
	 		* @description Suma todos los pagos de la cobranza actual para el cliente 
	 		* @returns {double} Suma total de los pagos
	 		*  
	 		*/
			$scope.obtenerSuma = function () {
				$scope.suma = 0;
				$scope.pagos.forEach(function (element) {
					$scope.suma += element.monto;
				});
				return $scope.suma;
			}
			/**
			* @ngdoc function
			* @name formatFecha
			*
			* @description Le da formato a una fecha para visualizar en pantalla
			* @example $scope.formatFecha("2015-05-24")
			* @param {String} fecha Fecha en formato "YYYY-MM-DD"
			* @returns {String} Fecha con formato
			*
			*/
			$scope.formatFecha = function (fecha) {
				return moment(fecha, "YYYY-MM-DD").locale('es').format("dddd, DD/MM");
			}
		}
		);