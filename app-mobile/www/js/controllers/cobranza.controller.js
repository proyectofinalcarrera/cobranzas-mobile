angular.module('cobranza-mobile.controllers')
	/**
	 * @ngdoc controller
	 * @name cobranza-mobile.controllers:CobranzaCtrl
	 * 
	 * @requires $scope
	 * @requires $filter
	 * @requires location
	 * @requires $rootScope
	 *
	 * @requires CobranzaService
	 * @requires ClienteService
	 * @requires PagoService
	 * 
	 * @description
	 * Controla la información y eventos de la vista de cobranza
	 */
	.controller('CobranzaCtrl', 
		function (
			$scope, $filter, $location, $rootScope,
			CobranzaService, ClienteService, PagoService, 
			moment, $ionicModal, $ionicPopup, $ionicActionSheet, $ionicHistory) {
			//Busca la cobranza actual para mostrar los datos por pantalla
			$scope.cobranza = CobranzaService.getCurrentCobranza();
			//Busca todos los pagos de la cobranza actual
			$scope.pagos = PagoService.listAll();		
			//Crea la ventana modal "Nuevo Pago" y la asigna a $scope.modal
			$ionicModal.fromTemplateUrl(
				'views/pago/new.pago.html', 
				{
					scope: $scope,
					animation: 'slide-in-up'
				}
			).then(
				function (modal) {
					$scope.modal = modal;
				}
			);
			/**
			* @ngdoc function
			* @name openModal
			*
			* @description Muestra la ventana modal
			*
			*/
			$scope.openModal = function () {
				//Valores iniciales del pago
				$scope.pago = 
				{
					clienteID: -1,
					monto : ""
				};
				var id = $scope.cobranza.abastoID;
				//Busca los clientes del abasto actual
				$scope.clientes = ClienteService.listAllFrom(id);
				//Se utiliza cuando se guarda el pago
				$scope.nuevo = true;
				$scope.modal.show();
			};
			/**
			* @ngdoc function
			* @name closeModal
			*
			* @description Cierra la ventanta modal
			*
			*/
			$scope.closeModal = function () {
				$scope.modal.hide();
			};
			/**
			* @ngdoc function
			* @name aceptarPago
			*
			* @description Crea/modifica un pago
			* 
			*/
			$scope.aceptarPago = function(){
				//Toma los parámetros
				var monto = Number($scope.pago.monto);
				var clienteKey = $scope.pago.clienteID;
				var mensaje = "";
				var validado = true;
				//Comienzo de validaciones
				if(isNaN(monto)){
					validado = false;
					mensaje = mensaje + "El monto ingresado no es válido.<br>";
				}
				if(monto === 0){
					validado = false;
					mensaje = mensaje + "Debe ingresar el monto.<br>";
				}
				if(clienteKey<0){
					validado = false;
					mensaje = mensaje + "Debe seleccionar un cliente.";	
				}
				//Fin de validaciones
				if(validado){
					var cliente = ClienteService.find(clienteKey);
					cliente.$loaded().then(
						/**
						 * @ngdoc function
						 *
						 * @description Crea un nuevo pago o modifica uno actual
						 * y luego cierra la ventana modal
						 */
						function(){
							if($scope.nuevo){
								var pago = 
								{
									monto: monto, 
									clienteID: cliente.ID, 
									clienteNombre: cliente.nombre + " " + cliente.apellido,
									precaucion: false 
								};
								PagoService.addPago(pago);
							}
							else{
								PagoService.editPago($scope.pago.$id,monto, cliente.ID,
									cliente.nombre + " " + cliente.apellido);				
							}
							$scope.closeModal();
						}
					);
				}
				else{
					$ionicPopup.alert(
					{
						title: "Datos incorrectos",
						template: mensaje
					}
					);
				}
				
			};
			/**
			 * @ngdoc function
			 * @name selected
			 *
			 * @description Selecciona un cliente. Se usa en la ventana modal
			 * @example $scope.selected(1)
			 * @param {int} clienteID ID del cliente que seleccionó el usuario
			 */
			$scope.selected = function(clienteID){
				$scope.pago.clienteID = clienteID;	
			};
			/**
			 * @ngdoc function
			 * @name cleanModal 
			 *
			 * @description Limpia los campos de la ventana modal
			 */			
			$scope.cleanModal = function(){
				$scope.pago.clienteID = -1;	
				$scope.pago.monto = "";
			};
			/**
			* @ngdoc function
			* @name mostrarOpcones
			*
			* @description Muesta un menú de opciones relacionadas al pago seleccionado
			* @example $scope.mostrarOpciones(1,{monto:200, clienteId: 2})
			* @param {String} pago Objeto que representa el pago seleccionado
			*
			*/
			$scope.mostrarOpciones = function (pago) {
				$ionicActionSheet.show(
				{
					buttons: 
						[
							{ text: '<center><b>Cuenta Corriente</b></center>' },
							{ text: '<center><b>Modificar</b></center>' }
						],
					destructiveText: '<center><b>Eliminar</b></center>',
					titleText: '<center>Opciones<center>',
					cancelText: 'Cancelar',
					cancel: function () {},
					buttonClicked: function (index) {
						switch (index) {
							//Opción "Cuenta Corriente" de cliente del pago
							case 0:
								//Redirige a la vista cuenta-corriente
								$location.url("app/cuenta-corriente/"+pago.clienteId);
								break;
							//Opción "Modificar" del pago
							case 1: 
								$scope.pago = pago;
								//Busca los clientes del abasto seleccionado en la cobranza
								//Se listan y el usuario debe seleccionar uno
								$scope.clientes = ClienteService.listAllFrom($scope.cobranza.abastoID);
								$ionicModal.fromTemplateUrl('views/pago/edit.pago.html',
								{
									scope: $scope,
									animation: 'slide-in-up'
								}
								).then(
									function (modal) {
										$scope.modal = modal;
										$scope.nuevo = false;
										$scope.modal.show();
									}
								);
								break;
						}
						return true;
					},
					destructiveButtonClicked:function(){
						PagoService.removePago(pago);
						return true;
					}
				});

			};
			/**
			* @ngdoc function
			* @name formatFecha
			*
			* @description Le da formato a una fecha para visualizar en pantalla
			* @example $scope.formatFecha("2015-05-24")
			* @param {String} fecha Fecha en formato "YYYY-MM-DD"
			* @returns {String} Fecha con formato
			*
			*/
			$scope.formatFecha = function(fecha){
				return moment(fecha, "YYYY-MM-DD").locale('es').format("dddd, L");	
				
			}
			/**
			 * @ngdoc function
			 * @name cancelarCobranza
			 * 
			 * @description Muestra un Popup solicitando la confirmación de la 
			 * cancelación. Si el usuario asiente, se cancela la cobranza actual
			 */
			$scope.cancelarCobranza = function(){
				//Muestra el Popup solicitando la confirmación de la cancelación
				$ionicPopup.alert({
						title: "Confirmar",
						template: "Presione OK para cancelar la cobranza actual",		
						buttons: 
							[
		  						{
		  							text: '<b>OK</b>',
		    						type: 'button-positive',
		    					onTap: function(e) {
		    						//Elimina el historial de la próxima vista
		    						//para que sea root
		    							$ionicHistory.nextViewOptions(
		    								{
		  										disableAnimate: true,
		  										disableBack: true
											}
										);
		      							CobranzaService.cancelarCobranza();
										//Oculta/muestra opciones del menú lateral
										$rootScope.existeCobranza = false;
										//Redirige a la vista cobranzas
										$location.url("app/cobranzas");
									
		    					}
		  					},
							  { text: 'No', onTap: function(e) {  }}
							 ]
						}
				);
				
			}		   
		}
	)
	/**
	 * @ngdoc controller
	 * @name cobranza-mobile.controllers:EditCobranzaCtrl
	 * 
	 * @requires $scope
	 * @requires $filter
	 * @requires location
	 *
	 * @requires CobranzaService
	 * @requires AbastoService
	 * 
	 * @description
	 * Controla la información y eventos de la edición de la cobranza actual
	 * Solo se edita la fecha
	 */
	.controller('EditCobranzaCtrl',
		function (
			$scope, $filter, $location, 
			CobranzaService, AbastoService,
			$ionicModal, $ionicPopup, moment) {
			//Instancia una ventana modal que permite editar la cobranza
			$ionicModal.fromTemplateUrl('views/cobranza/edit.cobranza.html', {
				scope: $scope,
				animation: 'slide-in-up'
				}).then(function (modal) {
				$scope.modal = modal;
			});
			/**
			 * @ngdoc function
			 * @name closeModal
			 * 
			 * @description Cierra la venta modal y limpia los campos
			 * 
			 */			
			$scope.closeModal = function () {
				$scope.modal.hide();
				$scope.cleanModal();
			};
			/**
			 * @ngdoc function
			 * @name modificarCobranza
			 * 
			 * @description Abre la ventana modal que permite modificar
			 * la información básica de la cobranza
			 */
			$scope.modificarCobranza = function(){
				//Busca la cobranza actual para completar los campos de la ventana
				$scope.cobranza = CobranzaService.getCurrentCobranza();
				$scope.cobranza.$loaded(function(){
					//La fecha se almacena como string
					//Se crea el objeto Date con la fecha de la cobranza
					$scope.fecha = new Date($scope.cobranza.fecha); 
				});
				//Muestra la ventana modal
				$scope.modal.show();
			}	
			/**
			 * @ngdoc function
			 * @name aceptarModificarCobranza
			 * 
			 * @description Modifica la fecha de la cobranza actual
			 */
			$scope.aceptarModificarCobranza = function(){
				//Obtiene el valor de la fecha seleccionada
				var fecha = $scope.fecha;
				//Le da el formato necesario a la fecha para almacenarla
				var stringDate = moment(fecha).locale('es').format('YYYY-MM-DD');
				var mensaje = "";
				var validado = true;
				//Valida que la feccha sea válida
				if(isNaN(fecha) || fecha === null){
					validado = false;
					mensaje = mensaje + "Debe ingresar una fecha.<br>";
				}
				if(validado){
					//Cierra la ventana modal
					$scope.closeModal();
					//Modifica la fecha
					CobranzaService.modificarCobranza(stringDate);
					//Se dirige a la cobranza actual
					$location.url("app/cobranza-actual");				
				}
				else{
					$ionicPopup.alert(
						{
							title: "Datos incorrectos",
							template: mensaje
						}
					);
				}
				
			};
			/**
			 * @ngdoc function
			 * @name cleanModal
			 * 
			 * @description Limpia los campos de la ventana modal
			 */
			$scope.cleanModal = function(){
				$scope.cobranza.abastoSelected = -1;
				$scope.cobranza.monto = "";
			};
			/**
			* @ngdoc function
			* @name formatFecha
			*
			* @description Le da formato a una fecha para visualizar en pantalla
			* @example $scope.formatFecha("2015-05-24")
			* @param {String} fecha Fecha en formato "YYYY-MM-DD"
			* @returns {String} Fecha con formato
			*
			*/
			$scope.formatFecha = function(fecha){
				return Date(fecha);
			}
			/**
			* @ngdoc function
			* @name cambiarFecha
			*
			* @description Cambia la fecha de la cobranza
			* @example $scope.formatFecha(new Date())
			* @param {String} fecha Fecha en formato Date
			*
			*/
			$scope.cambiarFecha = function(fecha){
				$scope.fecha = fecha;
			}
		});
