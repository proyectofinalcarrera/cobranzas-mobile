angular.module('cobranza-mobile.controllers')
	/**
	 * @ngdoc controller
	 * @name cobranza-mobile.controllers:ClientesCtrl
	 * 
	 * @requires $scope
	 * @requires $filter
	 *
	 * @requires ClienteService
	 * @requires CobranzaService
	 * 
	 * @description
	 * Controla la información y eventos de la vista clientes
	 */
	.controller('ClientesCtrl',
		function (
			$scope, $filter,
			ClienteService, CobranzaService) {
			//Busca los datos de la cobranza actual. La utiliza para
			//mostrar primero los clientes del abasto y luego el resto
			$scope.cobranzaActual = CobranzaService.getCurrentCobranza();
			//Busca los clientes
			$scope.clientes = ClienteService.listAll();
			//Propiedad que muestra/oculta la barra de búsqueda
			//Inicialmente empieza oculta
			$scope.hidden = true;
			//Datos de búsqueda. Inicialmente vacío
			$scope.data = {};
			/**
			* @ngdoc function
	 		* @name show
	 		*
	 		* @description Muestra la barra de búsqueda
	 		*  
	 		*/
			$scope.show = function () {
				$scope.hidden = false;
			};
			/**
			* @ngdoc function
	 		* @name hide
	 		*
	 		* @description Oculta la barra de búsqueda y 
	 		* borra los datos de búsqueda
	 		*  
	 		*/
			$scope.hide = function(){
				$scope.hidden = true;
				$scope.data.searchQuery = '';
			}
			/**
			* @ngdoc function
	 		* @name clearSearch
	 		*
	 		* @description Borra los datos de búsqueda
	 		*  
	 		*/
			$scope.clearSearch = function () {
				$scope.data.searchQuery = '';
			};
		}
	);